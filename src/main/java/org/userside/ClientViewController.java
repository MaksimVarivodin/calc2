/**
 * Sample Skeleton for 'clientview.fxml' Controller Class
 */

package org.userside;

import communicationData.MembersInfo;
import communicationData.SendInfo;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.net.ConnectException;
import java.util.HashSet;

public class ClientViewController {


    @FXML // fx:id="IpAdress"
    private TextField IpAdress; // Value injected by FXMLLoader

    @FXML // fx:id="Port"
    private TextField Port; // Value injected by FXMLLoader

    @FXML // fx:id="ipInfo"
    private Label ipInfo; // Value injected by FXMLLoader

    @FXML // fx:id="postInfo"
    private Label portInfo; // Value injected by FXMLLoader
    @FXML // fx:id="nickInfo"
    private Label nickInfo; // Value injected by FXMLLoader

    @FXML // fx:id="statusInfo"
    private Label statusInfo; // Value injected by FXMLLoader
    @FXML
    private TextField nickName;
    @FXML // fx:id="messageInput"
    private TextField messageInput; // Value injected by FXMLLoader
    @FXML // fx:id="membersInfo"
    private VBox membersInfo; // Value injected by FXMLLoader
    @FXML
    private VBox messageOutput;
    @FXML
    private Button sendMessage;
    private Client client= new Client("localhost", 7077, "NickName" );
    @FXML
    void selectAllRecipients(ActionEvent event) {
        client.getGui().selectAllMembers();
    }
    @FXML
    void connect(ActionEvent event) throws IOException {
        try{
            client.setGui(new ClientGUI(membersInfo, messageOutput, messageInput, sendMessage));
            if(client.startConnection()){
                statusInfo.setText("Connected to Server");
                client.waitForServerString().start();
                ipInfo.setText(client.getIPAddress());
                portInfo.setText(String.valueOf(client.getPort()));
            }
        }catch (ConnectException e){
            e.printStackTrace();
            statusInfo.setText("Connection Error");
            MembersInfo mi = new MembersInfo(new HashSet<>());
            mi.getMembers().add("Nickname");
            client.setMembers(mi);
        }

    }
    @FXML
    void sendMessage(ActionEvent event) throws IOException {
        client.sendNewMessage();
    }

    @FXML
    void fillServerInfo(ActionEvent event) {
        if (IpAdress.getText() != null && !IpAdress.getText().contentEquals("") ){
            ipInfo.setText(IpAdress.getText());
            client.setIPAddress(IpAdress.getText());
            client.endConnection();
            statusInfo.setText("disconnected from server");
        }

        if (Port.getText() != null && !Port.getText().contentEquals("") )
        {
            portInfo.setText(Port.getText());
            client.setPort(Integer.parseInt(Port.getText()));
            client.endConnection();
            statusInfo.setText("disconnected from server");
        }
        if (nickName.getText() != null && !nickName.getText().contentEquals("") )
        {
            nickInfo.setText(nickName.getText());
            client.setNickName(nickInfo.getText());
            client.endConnection();
            statusInfo.setText("disconnected from server");
        }

    }


}
