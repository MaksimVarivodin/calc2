package org.userside;

import communicationData.*;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.io.*;
import java.net.*;
import java.nio.channels.IllegalBlockingModeException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Client {
    private ClientGUI gui;
    private Socket socket;
    private PrintWriter output;
    private BufferedReader input;
    private String IPAddress;
    private int timeoutInSeconds = 0;
    private String nickName;
    private int Port;

    private MembersInfo members;

    public Client() {
        socket = new Socket();
    }

    public void setMembers(MembersInfo members) {
        this.members = members;
    }

    public Client(String IP) {
        this();
        this.IPAddress = IP;
    }

    public Client(String IP, int Port) {
        this(IP);
        this.Port = Port;
    }

    public Client(String IP, int Port, String nick) {
        this(IP, Port);
        this.nickName = nick;
    }
    public Client(String IP, int Port, String nick, ClientGUI gui) {
        this(IP, Port, nick);
        this.gui = gui;
    }

    public MembersInfo getInfo() {
        return members;
    }

    public int getPort() {
        return Port;
    }

    public String getIPAddress() {
        return IPAddress;
    }


    public void setPort(int port) {
        Port = port;
    }

    public void setIPAddress(String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public void setGui(ClientGUI gui) {
        this.gui = gui;
    }

    public ClientGUI getGui() {
        return gui;
    }

    public boolean startConnection() throws IOException, IllegalBlockingModeException, IllegalArgumentException {
        SocketAddress address = new InetSocketAddress(IPAddress, Port);
        socket.connect(address, timeoutInSeconds * 1000);
        output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
        input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        if (nickName != null && nickName.length() > 0) {
            output.println(nickName);
            output.flush();
        }
        return true;
    }

    public void endConnection() {
        if (socket.isConnected()) {
            try {
                socket.close();
                socket = new Socket();
            } catch (IOException e) {
                System.out.println("Connection ended");
            }

        }
    }

    public Thread waitForServerString() {
        return new Thread(() -> {
            try {
                String info;
                while ((info = input.readLine()) != null) {
                    try {
                        listMembers(info);
                        getNewLoopMessage(info);
                        getNewMessage(info);
                    } catch (ClassNotFoundException e) {
                        System.out.println("server sent not members");
                    } catch (IOException des) {
                        des.printStackTrace();
                    } catch (NoSuchMethodException e) {
                        throw new RuntimeException(e);
                    }

                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void listMembers(String serialized) throws IOException, ClassNotFoundException, NoSuchMethodException {
        MembersInfo membersInfo = Serializator.<MembersInfo>Deserialize(serialized, MembersInfo.type());
        if (membersInfo != null) {
            members = membersInfo;
            gui.runFill(members, nickName, loopDialogue());
        }
    }
    public void getNewMessage(String serialized) throws IOException, ClassNotFoundException {
        Message message = Serializator.Deserialize(serialized, Message.type());
        if(message!= null){
            gui.printMessage(message, true);
        }
    }
    public void sendNewMessage() throws IOException {
        SendInfo sendInfo =  gui.checkSendMembers();
        sendInfo.setMessage(gui.getTextMessage());
        if(!sendInfo.getMessage().isEmpty()&& sendInfo != null){
            sendInfo.setSender(nickName);
            sendMessage(sendInfo, SendInfo.type());
            gui.printMessage(sendInfo, false);
        }

    }
    public void getNewLoopMessage(String serialized) throws IOException, ClassNotFoundException {
        Message message = Serializator.<Message>Deserialize(serialized, "[LP]");
        if (message != null) {
            gui.printLoopMessage(message, true);
            if (!message.getSender().contentEquals(nickName)) {
                sendLoopMessageToNext(message);
            }
        }
    }

    public Thread sendLoopThread(Message message){
        return new Thread(() -> {
            try {
                Thread.sleep(5000);
                sendMessage(message, "[LP]");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException i) {
                i.printStackTrace();
            }
        });
    }

    public void sendLoopMessageToNext(Message message) throws IOException {
        message.setReceiver(new HashSet<>(Collections.singleton(members.getNext(nickName))));
        gui.printLoopMessage(message, false);
        sendLoopThread(message).start();
    }

    public EventHandler<MouseEvent> loopDialogue() {
        return new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getButton() == MouseButton.SECONDARY) {
                    if (!members.getMembers().isEmpty()) {
                        LoopMessageDialog loop = new LoopMessageDialog(members);
                        try {
                            // public Message(String from, String to, String text);
                            String text = gui.getTextMessage();
                            Message message = new Message(nickName, loop.start(), text);
                            loop.close();
                            if (!message.getMessage().isEmpty() && !message.getReceiver().isEmpty()) {
                                gui.printLoopMessage(message, false);
                                sendMessage(message, "[LP]");
                            }
                        } catch (IOException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }
        };
    }

    public synchronized <T extends Sendable> void sendMessage(T data, String type) throws IOException {
        String value = Serializator.<T>Serialize(data, type);
        if (value != null && value.length() > 0) {
            output.println(value);
            output.flush();
        }
    }

}

