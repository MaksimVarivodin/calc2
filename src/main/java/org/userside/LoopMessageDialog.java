package org.userside;

import communicationData.MembersInfo;
import communicationData.Message;
import communicationData.SendInfo;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;

import java.util.Optional;

public class LoopMessageDialog extends Dialog<ButtonType> {
    private ToggleGroup group = new ToggleGroup();
    public ObservableList names = FXCollections.observableArrayList();

    private class RadioListCell extends ListCell<String> {
        @Override
        public void updateItem(String obj, boolean empty) {
            super.updateItem(obj, empty);
            if (empty) {
                setText(null);
                setGraphic(null);
            } else {
                RadioButton radioButton = new RadioButton(obj);
                radioButton.setToggleGroup(group);
                // Add Listeners if any
                setGraphic(radioButton);
            }
        }
    }
    private MembersInfo info;
    private VBox root;
    private ButtonType send;
    private ButtonType cancel;
    LoopMessageDialog(){
        super();
        send = new ButtonType("Send");
        cancel = new ButtonType("Cancel", ButtonBar.ButtonData.CANCEL_CLOSE);

    }

    LoopMessageDialog (MembersInfo sI){
        this();
        this.info = sI;
        setUI();
    }
    private void setUI(){
        super.setTitle("Select Recievers");
        DialogPane dialogPane = new DialogPane();
        ListView inner = new ListView();
        root = new VBox();
        root.setAlignment(Pos.CENTER);
        ToggleGroup group = new ToggleGroup();
        if (info!= null && !info.getMembers().isEmpty() ){
            for (String el:
                    info.getMembers()) {
                names.add(el);
            }
        }
        inner.setItems(names);
        inner.setCellFactory(param -> {
            RadioListCell radioListCell = new RadioListCell();
            return radioListCell;
        });
        inner.setMinHeight(100);
        root.getChildren().add(inner);
        dialogPane.getButtonTypes().addAll(send, cancel);
        dialogPane.setContent(root);
        super.setDialogPane(dialogPane);
    }
    public String start(){
        Optional<ButtonType> result = super.showAndWait();
        try{
            RadioButton selectedToggle = (RadioButton) group.getSelectedToggle();
            if (result.get() == send) return selectedToggle.getText();
        }finally {
            super.close();
        }
        return null;

    }
}
