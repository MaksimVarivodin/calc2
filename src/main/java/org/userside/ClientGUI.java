package org.userside;

import communicationData.MembersInfo;
import communicationData.Message;
import communicationData.SendInfo;
import communicationData.Sendable;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

public class ClientGUI {
    private final static String
            loopColor =
            "-fx-color: rgb(255,255,255);"
                    + "-fx-background-color: rgb(255,0,0);"
                    + "-fx-background-radius:20px;",
            receivedColor =
                    "-fx-color: rgb(0, 0, 0);"
                            + "-fx-background-color: rgb(233, 233, 235);"
                            + "-fx-background-radius:20px;",
            sentColor =
                    "-fx-color: rgb(329, 242, 255);"
                            + "-fx-background-color: rgb(15, 125, 242);"
                            + "-fx-background-radius: 20px";
    private VBox people;
    private VBox chat;
    private TextField input;
    private Button send;

    public ClientGUI() {
    }

    public ClientGUI(VBox people, VBox chat, TextField input, Button send) {
        this.people = people;
        this.chat = chat;
        this.input = input;
        this.send = send;
    }

    public void setMembers(VBox members) {
        this.people = members;
    }

    public void setChat(VBox chat) {
        this.chat = chat;
    }

    public void setInput(TextField input) {
        this.input = input;
    }

    public void setSend(Button send) {
        this.send = send;
    }

    public void printLoopMessage(Sendable message, boolean received) {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(5, 5, 5, 10));
        Label label = new Label(message.getMessage());
        label.setPadding(new Insets(5, 10, 5, 10));
        Tooltip tooltip = new Tooltip("Sent to:\n".concat(message.receiverToString()).concat("\nFrom:").concat(message.getSender()));
        label.setTooltip(tooltip);
        label.setStyle(loopColor);
        if (received)
            hbox.setAlignment(Pos.CENTER_LEFT);
        else
            hbox.setAlignment(Pos.CENTER_RIGHT);
        hbox.getChildren().add(label);
        RunLater(chat, hbox);
    }

    public void printMessage(Sendable message, boolean received) {
        HBox hbox = new HBox();
        hbox.setPadding(new Insets(5, 5, 5, 10));
        Label label = new Label(message.getMessage());
        label.setPadding(new Insets(5, 10, 5, 10));
        Tooltip tooltip = new Tooltip("Sent to:\n".concat(message.receiverToString()).concat("\nFrom:").concat(message.getSender()));
        label.setTooltip(tooltip);
        if (received) {
            label.setStyle(receivedColor);
            hbox.setAlignment(Pos.CENTER_LEFT);
        } else {
            label.setStyle(sentColor);
            hbox.setAlignment(Pos.CENTER_RIGHT);
        }
        hbox.getChildren().add(label);
        RunLater(chat, hbox);
    }

    public void selectAllMembers() {
        for (Node element : people.getChildren()) {
            if (element.getClass() == CheckBox.class) {
                CheckBox ch = (CheckBox) element;
                ch.setSelected(true);
            }
        }
    }

    public SendInfo checkSendMembers() {
        SendInfo sI = new SendInfo();
        Set<String> recipients = new HashSet<String>();
        ObservableList<Node> list = people.getChildren();
        for (Node element : list) {
            if (element.getClass() == CheckBox.class) {
                CheckBox ch = (CheckBox) element;
                if (ch.isSelected()&& !ch.isDisable()) {
                    recipients.add(ch.getText());
                }
            }
        }
        if (!recipients.isEmpty()){
            sI.setReceiver(recipients);
            return sI;
        }
        return null;
    }

    public <T extends Node> void RunLater(Pane el, T val) {
        if (el != null && val != null) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    el.getChildren().add(val);
                }
            });
        }
    }

    public void runFill(MembersInfo members, String nickName, EventHandler<MouseEvent> loop) {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (members != null && members.getMembers().size() > 0) {
                    people.getChildren().clear();
                    people.setSpacing(10);
                    people.setAlignment(Pos.TOP_LEFT);
                    people.setPadding(new Insets(5, 5, 5, 10));
                    for (String memb : members.getMembers()) {
                        if (memb.contentEquals(nickName)){
                            memb += "(Me)";
                            CheckBox ch = new CheckBox(memb);
                            ch.setIndeterminate(true);
                            ch.setDisable(true);
                            ch.setSelected(false);

                            people.getChildren().add(new CheckBox(memb));
                        } else  people.getChildren().add(new CheckBox(memb));
                    }
                    send.addEventHandler(MouseEvent.MOUSE_CLICKED, loop);
                }
            }
        });
    }


    public String getTextMessage() {
        if (input != null && !input.getText().isEmpty()) {
            String text = input.getText();
            input.clear();
            return text;
        }
        return null;
    }

}
