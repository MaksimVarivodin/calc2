package communicationData;

import java.io.*;
import java.util.Base64;
import java.util.Stack;

public class Serializator{
    public static<T> String Serialize(T toServerObject, String type) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream( baos );
        oos.writeObject(toServerObject);
        oos.close();
        return type + Base64.getEncoder().encodeToString(baos.toByteArray());
    }
    public static<T extends Serializable> T Deserialize (String fromSenderString, String type) throws IOException, ClassNotFoundException {
        if (fromSenderString.substring(0, 4).contentEquals(type)){
            fromSenderString = fromSenderString.substring(4, fromSenderString.length());
            byte[] data = Base64.getDecoder().decode(fromSenderString);
            ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
            T serverdata = (T)(ois.readObject());
            ois.close();
            return serverdata;
        } else {
            return null;
        }

    }
}
