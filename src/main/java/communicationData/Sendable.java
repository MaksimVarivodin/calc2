package communicationData;

import java.io.Serializable;
import java.util.Set;

public interface Sendable extends Serializable {

    String getSender();

    Set<String> getReceiver();

    String getMessage();

    void setSender(String sender);

    void setReceiver(Set<String> receiver);

    void setMessage(String message);

    String receiverToString();

    static String type() {
        return null;
    }

    void addReceiver(String receiver);


}