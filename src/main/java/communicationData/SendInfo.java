package communicationData;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import communicationData.Message;

public class SendInfo implements Sendable {
    protected Set<String> receivers;
    protected String sender;
    protected String message;

    public boolean ReceiversSelected() {
        return receivers.size() > 0;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String receiverToString() {

        if (receivers != null) {
            String res = "";
            for (String string :
                    receivers) {
                res = res.concat(string).concat("\n");
            }
            return res;
        }
        return null;

    }

    public String getMessage() {
        return message;
    }

    public String getSender() {
        return sender;
    }

    @Override
    public Set<String> getReceiver() {
        return receivers;
    }

    public boolean deleteReceiver(String selectedUser) {
        if (!sender.contentEquals(selectedUser)) {
            if (receivers.remove(selectedUser)) {
                return true;
            }
        }
        return false;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    @Override
    public void setReceiver(Set<String> receiver) {
        receivers = receiver;
    }

    public SendInfo() {
        receivers = new HashSet<String>();
    }

    public SendInfo(Set<String> receivers) {
        this.receivers = receivers;
    }

    public SendInfo(Set<String> receivers, String message, String sender) {
        this(receivers);
        this.sender = sender;
        this.message = message;
    }

    public static String type() {
        return "[SI]";
    }

    @Override
    public void addReceiver(String receiver) {
        receivers.add(receiver);
    }


    public static Message Transform(SendInfo val) {
        Message message1 = new Message();
        message1.setReceiver(val.getReceiver());
        message1.setMessage(val.getMessage());
        message1.setSender(val.getSender());
        return message1;
    }
}
