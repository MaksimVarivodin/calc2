package communicationData;

import java.io.Serializable;
import java.util.*;

public class MembersInfo implements Serializable {
    private Set<String> members;

    public void setMembers(Set<String> members) {
        this.members = members;
    }

    public MembersInfo() {
        members = new HashSet<>();
    }

    public MembersInfo(Set<String> members) {
        this.members = members;
    }

    public Set<String> getMembers() {
        return members;
    }

    public void addMember(String member) {
        members.add(member);
    }

    public void removeMember(String member) {
        members.remove(member);
    }

    public static String type() {
        return "[MI]";
    }
    public String getNext(String name){
        String[]array = new String[members.size()];
        members.<String>toArray(array);
        List<String> buffer= new ArrayList<>(List.of(array));
        int i= buffer.indexOf(name);
        if (i+ 1 < buffer.size()){
            return buffer.get(i+1);
        }else{
            return buffer.get(0);
        }
    }
}
