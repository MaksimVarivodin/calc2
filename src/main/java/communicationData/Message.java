package communicationData;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

public class Message implements Sendable {
    String from;
    String to;
    String text;

    public Message(String text) {
        this.text = text;
    }

    public Message(String from, String to, String text) {
        this(text);
        this.from = from;
        this.to = to;
    }

    public Message() {

    }
    public static String type() {
        return "[MS]";
    }

    @Override
    public void addReceiver(String receiver) {
        to = receiver;
    }

    @Override
    public String getSender() {
        return from;
    }

    @Override
    public Set<String> getReceiver() {
        Set<String> set = new HashSet<>();
        set.add(to);
        return set;
    }

    @Override
    public String getMessage() {
        return text;
    }

    @Override
    public void setSender(String sender) {
        from = sender;
    }

    @Override
    public void setReceiver(Set<String> receiver) {
        String[] buffer = new String[receiver.size()];
        receiver.toArray(buffer);
        if (receiver.size() == 1) {
            to = buffer[0];
        } else if (receiver.size() > 1) {
            to = buffer[receiver.size() - 1];
        }
    }

    @Override
    public void setMessage(String message) {
        text = message;
    }

    @Override
    public String receiverToString() {
        return to;
    }
    public static SendInfo Transform(Message val) {
        SendInfo message1 = new SendInfo();
        message1.setReceiver(val.getReceiver());
        message1.setMessage(val.getMessage());
        message1.setSender(val.getSender());
        return message1;
    }
}
