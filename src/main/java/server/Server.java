package server;

import communicationData.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.Set;

public class Server {
    private static final int PORT = 7077;
    private static final Set<PrintWriter> writers = new HashSet<>();

    private static final MembersInfo members = new MembersInfo();
    private static final Set<Handler> users = new HashSet<>();

    public static void main(String[] args) throws Exception {
        try (ServerSocket listener = new ServerSocket(PORT, 100)) {
            listener.setSoTimeout(0);
            System.out.println("The chat server is running...");
            //noinspection InfiniteLoopStatement
            while (true) {
                new Handler(listener.accept()).start();
            }
        }
    }

    private static class Handler extends Thread {
        private final Socket socket;
        private String name;
        private BufferedReader in;
        private PrintWriter out;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        public String getUserName() {
            return name;
        }

        public PrintWriter getWriter() {
            return out;
        }

        public void run() {
            try {
                // Создаем потоки ввода/вывода для обмена данными с клиентом
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);

                // Запрашиваем имя клиента и отправляем ему сообщение приветствия
                name = in.readLine();
                System.out.println(name);
                synchronized (members) {
                    members.addMember(name);
                }
                synchronized (users) {
                    users.add(this);
                }
                // Добавляем PrintWriter текущего клиента в список
                synchronized (writers) {
                    writers.add(out);
                }
                // Отправляем всем новый список пользователей
                synchronized (writers) {
                    for (PrintWriter writer : writers) {
                        writer.println(Serializator.Serialize(members, MembersInfo.type()));
                    }
                }

                // Читаем сообщения от клиента и отправляем их всем остальным подключенным клиентам
                String input;
                while ((input = in.readLine()) != null) {
                    Message message = null;
                    SendInfo sendInfo = null;
                    String lp = "[LP]",
                            ms = Message.type(),
                            si = SendInfo.type(), res = null;
                    try {
                        if((message = Serializator.Deserialize(input, lp))== null){
                            if((message = Serializator.Deserialize(input, ms))== null){
                                if((sendInfo = Serializator.Deserialize(input, si))==null){
                                    continue;
                                }else message = SendInfo.Transform(sendInfo);
                            }else {sendInfo = Message.Transform(message);res = ms;}
                        }else {sendInfo = Message.Transform(message);res = lp;}


                    } catch (ClassNotFoundException e) {
                        System.out.println("Can't deserialize message");
                    }

                    System.out.println(message.getMessage());
                    synchronized (writers) {
                        synchronized (users) {
                            for (Handler user : users) {
                                for(String rec: sendInfo.getReceiver()){
                                    if (user.getUserName().equals(rec)) {
                                        message.addReceiver(user.getUserName());
                                        if(res!= null)
                                        user.getWriter().println(Serializator.Serialize(message, res));
                                        else user.getWriter().println(Serializator.Serialize(message, ms));
                                    }
                                }

                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                // Если клиент отключается, удаляем его PrintWriter из списка
                if (out != null) {
                    synchronized (writers) {
                        writers.remove(out);
                        synchronized (members) {
                            members.removeMember(name);
                        }
                        // Новый список пользователей
                        for (PrintWriter writer : writers) {
                            try {
                                writer.println(Serializator.Serialize(members, MembersInfo.type()));
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                    synchronized (users) {
                        users.remove(this);
                    }
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
