module org.userside {
    requires javafx.controls;
    requires javafx.fxml;

    opens org.userside to javafx.fxml;
    exports org.userside;
}
